package question.trois.dix;

import java.math.BigInteger;

import question.un.Echauffement;
import tasBinomial.TasBinomial;

public class ConsIterFileBinomial {

	Echauffement echauffement;

	public ConsIterFileBinomial() {
		echauffement = new Echauffement();
	}

	public String complexiteMoyenne(int nbCle) {

		long time = 0;
		long time_before;
		long time_after;

		TasBinomial tasBinomial = new TasBinomial();

		for (int i = 0; i < 5; i++) {

			String fileName = "src/cles_alea/jeu_" + (i + 1) + "_nb_cles_" + nbCle + ".txt";

			BigInteger[] liste = echauffement.lecteurFichier(fileName);

			time_before = System.nanoTime();

			tasBinomial.consIter(liste, nbCle);

			time_after = System.nanoTime();

			long x = (time_after - time_before) / 1000;

			time = time + x;

		}

		time = time / 5;

		return time + "";
	}

	public static void main(String[] args) {

		int[] tailles = {100,200,500,1000,5000,10000,20000,50000};
		
		String [] resulat = new String[8] ;

		ConsIterFileBinomial complex = new ConsIterFileBinomial();
		
		for(int i = 0 ; i < tailles.length ; i++) {
	        
			resulat[i] = complex.complexiteMoyenne(tailles[i]);
		}
		
		System.out.println("\n----------- complexité ConsIter file Binomial : -----------\n");
		
		for(int i  = 0 ; i < tailles.length ; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulat[i]+" microseconds ");
			System.out.print("\n--------------------------------------------------------------------------------------\n");
		}

		
	}

}
