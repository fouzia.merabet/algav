package question.cinq;
import java.math.BigInteger;

import question.cinq.NoeudAVL;
public class ArbreAVL {
	private NoeudAVL racine;
	public ArbreAVL(BigInteger x) {
		
	}
	   private boolean equilibreD ( NoeudAVL r, NoeudAVL p, boolean g){
		// r est le fils gauche de p si g vaut true, r est le fils droit de p si g vaut false
		   // retourne true si après équilibrage l'arbre a grandi
		  NoeudAVL r1, r2;
		  switch(r.equilibre){
		      case -1 : r.equilibre = 0; return false;
		      case  0 : r.equilibre = 1; return true;
		      case  1 :
		      default : r1 = r.droit;
			        if(r1.equilibre == 1){
				   r.droit = r1.gauche; r1.gauche = r;
				   r.equilibre = 0;
				   r = r1 ;
				}else{
				   r2 = r1.gauche; r1.gauche = r2.droit;
				   r2.droit = r1;
				   r.droit = r2.gauche; r2.gauche = r;
				   if(r2.equilibre == 1) r.equilibre = -1;
				   else                  r.equilibre = 0;
				   if(r2.equilibre == -1) r1.equilibre = 1;
				   else                   r1.equilibre = 0;
				   r = r2;
				}
				// refaire le chaînage avec le père
				if(p==null) racine = r;
				else if( g ) p.gauche = r ;
				     else p.droit = r ;
				r.equilibre = 0; 
				return false;
		   }
		}

		
		boolean equilibreG (NoeudAVL r, NoeudAVL p, boolean g){ 
		     // r est le fils gauche de p si g vaut  true, r est le fils droit de p si g vaut false
		     // retourne true si après équilibrage l'arbre a grandi 
		     NoeudAVL r1, r2; 
		     switch (r.equilibre){
		         case 1 : r.equilibre=0; return false;  
		         case 0 : r.equilibre = -1; return true;  
		         case -1 :
		         default : r1 = r.gauche;
			     if(r1.equilibre==-1){
			        r.gauche = r1.droit; r1.droit= r; 
			        r.equilibre = 0; r = r1;
			     }else{ 
			        r2 = r1.droit; r1.droit = r2.gauche; r2.gauche=r1; 
			        r.gauche=r2.droit; r2.droit = r; 
			        if(r2.equilibre == -1) r.equilibre = 1; 
			        else               r.equilibre = 0;
			        if(r2.equilibre == 1) r1.equilibre =-1; 
			        else               r1.equilibre = 0; 
			        r=r2;
			   }
			   // refaire le chaînage avec le père 
			   if (p == null) racine = r;
			   else if( g ) p.gauche = r ; 
			        else     p.droit = r ; 
			   r.equilibre = 0; 
			   return false;
		   } 
		}


		void ajouter ( Comparable x){
		    ajoutAVL( racine, null, true, x);
		}

		boolean ajoutAVL(NoeudAVL r, NoeudAVL p, boolean g, Comparable e){
		   if(r == null) {
		       r = new NoeudAVL(e, null, null);
		       if (p == null) racine  = r;
		       else if(g) p.gauche = r;
		            else  p.droit = r;
		       return true;
		   }else{
		      int a = e.compareTo(r.element);
		      if (a==0) return false;// a déjà présent dans l'arbre
		      if (a<0)
		          if(ajoutAVL(r.gauche, r, true, e)) return equilibreG(r, p, g);
		          else return false;
		      else
		          if(ajoutAVL(r.droit, r, false, e)) return equilibreD(r, p, g);
		          else return false;
		   }
	    }
	  
}
