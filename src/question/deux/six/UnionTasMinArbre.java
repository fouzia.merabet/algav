package question.deux.six;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import Tas_minarbre.CalculComplexiteUnion;
import Tas_minarbre.CalculeComplexite;
import Tas_minarbre.Tas_min;
import Tas_minarbre.echauf;

public class UnionTasMinArbre {
	
	


	public String complexiteMoyenne(int nbCle) {
		
		long time_before ; 
		long time_after ;
	
		Tas_min tas=new Tas_min();
		Tas_min tas1=new Tas_min();
		Tas_min T=new Tas_min();
		List<BigInteger> list1 = new ArrayList<BigInteger>();
		List<BigInteger> list2 = new ArrayList<BigInteger>();
		
		list1 =echauf.lecteur("src/cles_alea/jeu_1_nb_cles_" + nbCle + ".txt") ; 
		list2 = echauf.lecteur("src/cles_alea/jeu_2_nb_cles_" + nbCle + ".txt") ; 
					
		tas.construction(list1);
		
		tas1.construction(list2);
		

		time_before = System.nanoTime();
		
		 T=tas.union(tas1);
		
		time_after = System.nanoTime();
						
		long x = (time_after - time_before)/1000; 
	
			return x+""; 
		}
	
	public static void main(String[] args) {
		

		int[] tailles = { 100, 200, 500, 1000, 5000, 10000, 20000, 50000 };

		String[] resulat = new String[8];

		UnionTasMinArbre complex = new UnionTasMinArbre();

		for (int i = 0; i < tailles.length; i++) {
			resulat[i] = complex.complexiteMoyenne(tailles[i]);
		}

		System.out.println("\n----------- complexité Union tas min via Arbre: -----------\n");

		for (int i = 0; i < tailles.length; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulat[i]+" milliseconds");
			System.out.print("\n--------------------------------------------------------------------------------------\n");
		
		}

		
	}


}
