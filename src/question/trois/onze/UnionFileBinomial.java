package question.trois.onze;

import java.math.BigInteger;

import question.un.Echauffement;
import tasBinomial.TasBinomial;


public class UnionFileBinomial {
	
	
	public void testMethod(int nb) {
		for(int i = 0 ; i < nb ; i++) {
			System.out.print("");
		}
	}


	Echauffement echauffement;

	public UnionFileBinomial() {
		echauffement = new Echauffement();
	}
	
	public String complexiteMoyenne(int nbCle) {
		
		long time_before ; 
		long time_after ;
		

		TasBinomial tasBinomial1 = new TasBinomial(); 
		
		TasBinomial tasBinomial2 = new TasBinomial(); 
		
		String fileName1 = "src/cles_alea/jeu_1_nb_cles_"+nbCle+".txt" ; 
		String fileName2 = "src/cles_alea/jeu_4_nb_cles_"+nbCle+".txt" ; 
				
		BigInteger[] liste1 = echauffement.lecteurFichier(fileName1);
		BigInteger[] liste2 = echauffement.lecteurFichier(fileName2);
				
		tasBinomial1.consIter(liste1, nbCle);
		tasBinomial2.consIter(liste2, nbCle);
		

		time_before = System.nanoTime();

		tasBinomial1.union(tasBinomial2.getNoeuds());
		
		time_after = System.nanoTime();
						

			return ((time_after - time_before)/1000)+""; 
		}
	
	
	public static void main(String[] args) {

		
	    
	    
	    UnionFileBinomial complex = new UnionFileBinomial();
	    		
		
		int[] tailles = { 100, 200, 500, 1000, 5000, 10000, 20000, 50000 };

		String[] resulat = new String[8];

		
		
		for (int i = 0; i <tailles.length ; i++) {
			resulat[i] = complex.complexiteMoyenne(tailles[i]);
		}

		System.out.println("\n----------- complexité Union file Binomial : -----------\n");

		for (int i = 0; i < tailles.length; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulat[i]+" microseconds ");
			System.out.print("\n--------------------------------------------------------------------------------------\n");
		}
		
	
		
		
		
		
		
		
		

	}
	
	
}
