package tasBinomial;

import java.math.BigInteger;
import java.util.concurrent.TimeUnit;

import question.un.*;;

public class TasBinomial {

	private int nbMsToSleep;

	public int getNbMsToSleep() {
		return nbMsToSleep;
	}

	public void setNbMsToSleep(int nbMsToSleep) {
		this.nbMsToSleep = nbMsToSleep;
	}

	private ArbreBinomial noeuds;

	public void setTaille(int taille) {
		this.taille = taille;
	}

	private int taille;

	public TasBinomial() {
		noeuds = null;
		taille = 0;
		nbMsToSleep = 100;
	}

	public ArbreBinomial getNoeuds() {
		return noeuds;
	}

	public void setNoeuds(ArbreBinomial noeuds) {
		this.noeuds = noeuds;
	}

	public boolean estVide() {
		return noeuds == null;
	}

	public int getTaille() {
		return taille;
	}

	public void vider() {
		noeuds = null;
		taille = 0;
	}

	public void ajout(BigInteger value) {
		if (Echauffement.sup(value, new BigInteger("0"))) {
			ArbreBinomial temp = new ArbreBinomial(value);
			if (noeuds == null) {

				noeuds = temp;
				taille = 1;
			} else {
				union(temp);
				taille++;
			}
		}
	}

	public void ajout2(BigInteger value) {
		if (Echauffement.sup(value, new BigInteger("0"))) {
			ArbreBinomial temp = new ArbreBinomial(value);
			if (noeuds == null) {
				noeuds = temp;
				taille = 1;
			} else {
				union(temp);
				taille++;
			}
		}
	}

	private void fusionner(ArbreBinomial binHeap) {
		ArbreBinomial temp1 = noeuds, temp2 = binHeap;

		while ((temp1 != null) && (temp2 != null)) {

			if (temp1.getDegree() == temp2.getDegree()) {
				ArbreBinomial tmp = temp2;
				temp2 = temp2.getFrere();
				tmp.setFrere(temp1.getFrere());
				temp1.setFrere(tmp);
				temp1 = tmp.getFrere();
			} else {
				if (temp1.getDegree() < temp2.getDegree()) {
					if ((temp1.getFrere() == null) || (temp1.getFrere().getDegree() > temp2.getDegree())) {
						ArbreBinomial tmp = temp2;
						temp2 = temp2.getFrere();
						tmp.setFrere(temp1.getFrere());
						temp1.setFrere(tmp);
						temp1 = tmp.getFrere();
					} else {
						temp1 = temp1.getFrere();
					}
				} else {
					ArbreBinomial tmp = temp1;
					temp1 = temp2;
					temp2 = temp2.getFrere();
					temp1.setFrere(tmp);
					if (tmp == noeuds) {
						noeuds = temp1;
					}
				}
			}
		}
		if (temp1 == null) {
			temp1 = noeuds;
			while (temp1.getFrere() != null) {
				temp1 = temp1.getFrere();
			}
			temp1.setFrere(temp2);
		}
	}

	public void union(ArbreBinomial binHeap) {

	    try {
			Thread.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		fusionner(binHeap);

		ArbreBinomial prevTemp = null, temp = noeuds, nextTemp = noeuds.getFrere();

		while (nextTemp != null) {

			if ((temp.getDegree() != nextTemp.getDegree())
					|| ((nextTemp.getFrere() != null) && (nextTemp.getFrere().getDegree() == temp.getDegree()))) {
				prevTemp = temp;
				temp = nextTemp;
			} else {
				if (Echauffement.infOrEq(temp.getCle(), nextTemp.getCle())) {
					temp.setFrere(nextTemp.getFrere());
					nextTemp.setParent(temp);
					nextTemp.setFrere(temp.getEnfant());
					temp.setEnfant(nextTemp);
					temp.setDegree(temp.getDegree() + 1);
				} else {
					if (prevTemp == null) {
						noeuds = nextTemp;
					} else {
						prevTemp.setFrere(nextTemp);
					}
					temp.setParent(nextTemp);
					temp.setFrere(nextTemp.getEnfant());
					nextTemp.setEnfant(temp);
					nextTemp.setDegree(nextTemp.getDegree() + 1);
					temp = nextTemp;
				}
			}
			nextTemp = temp.getFrere();
		}
	}

	// checked
	public ArbreBinomial trouveMin() {

		// return noeuds.minDeg().getCle();

		ArbreBinomial x = noeuds, y = noeuds;
		BigInteger min = x.getCle();

		while (x != null) {
			if (Echauffement.inf(x.getCle(), min)) {
				y = x;
				min = x.getCle();
			}
			x = x.getFrere();
		}

		return y;
	}

	public BigInteger supprMin() {
		if (noeuds == null)
			return new BigInteger("-1");

		ArbreBinomial temp = noeuds, prevTemp = null;
		ArbreBinomial minNode = trouveMin();

		while (!Echauffement.eg(temp.getCle(), minNode.getCle())) {
			prevTemp = temp;
			temp = temp.getFrere();
		}

		if (prevTemp == null) {

			noeuds = temp.getFrere();
		} else {
			prevTemp.setFrere(temp.getFrere());
		}

		temp = temp.getEnfant();
		ArbreBinomial fakeNode = temp;

		while (temp != null) {
			temp.setParent(null);
			temp = temp.getFrere();
		}

		if ((noeuds == null) && (fakeNode == null)) {
			taille = 0;
		} else {
			if ((noeuds == null) && (fakeNode != null)) {
				noeuds = fakeNode.inverser(null);
				taille = noeuds.getTaille();
			} else {
				if ((noeuds != null) && (fakeNode == null)) {
					taille = noeuds.getTaille();
				} else {
					union(fakeNode.inverser(null));
					taille = noeuds.getTaille();
				}
			}
		}

		return minNode.getCle();
	}

	public void consIter(BigInteger listeElement[], int taille) {

		for (int i = 0; i < taille; i++) {

			ajout(listeElement[i]);
		}

	}

	public void displayHeap() {
		System.out.print("\n-- File Binomial : ");
		displayHeap(noeuds);
		System.out.println("\n");
	}

	private void displayHeap(ArbreBinomial r) {
		if (r != null) {
			displayHeap(r.getEnfant());
			System.out.print(r.getCle() + " | ");
			displayHeap(r.getFrere());
		}
	}
}
