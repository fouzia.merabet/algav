package question.six.quatorze;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;

import question.un.Echauffement;
import tasBinomial.TasBinomial;
import tatPriorite.min.TasMin;

public class ComplexConsIter {
	
	
	Echauffement echauffement  ;
	
	public  ComplexConsIter() {
		echauffement = new Echauffement() ;
	}	
	
	public String complexConsIterTasMin(int nbCle) {
		
		long time = 0 ; 
		long time_before ; 
		long time_after ;
		
		TasMin tasMin = new TasMin(); 

							
				String fileName = "src/cles_alea/jeu_1_nb_cles_"+nbCle+".txt" ; 
				
				BigInteger[] liste = echauffement.lecteurFichier(fileName);
				
				time_before = System.nanoTime();

				tasMin.setNombrePercolation(0);
				
				tasMin.consIter1(liste, nbCle);
				
				time_after = System.nanoTime();
				
						
				long x = (time_after - time_before)/1000; 
				
				time = time + x ;
				
					
			time = time / 5;
					
			return time+""; 
		}
	
	
	
	public String complexConsIterTasBinomial(int nbCle) {
		
		long time = 0 ; 
		long time_before ; 
		long time_after ;
		
		TasBinomial tasBinomial = new TasBinomial(); 
							
				String fileName = "src/cles_alea/jeu_1_nb_cles_"+nbCle+".txt" ; 
				
				BigInteger[] liste = echauffement.lecteurFichier(fileName);
								
				time_before = System.nanoTime();

				tasBinomial.consIter(liste, nbCle);
				
				time_after = System.nanoTime();
						
				long x = (time_after - time_before)/1000; 
				
				time = time + x ;
									
			time = time / 5;
					
			return time+""; 
		}
	
	
	
	
	public static void main(String[] args) {
		
		

		String [] resulatTasMin = new String[8] ;
		
		String [] resulatFileBinomial = new String[8] ;
		
		int[] tailles = {100,200,500,1000,5000,10000,20000,50000};
		
		ComplexConsIter complex = new ComplexConsIter(); 
			
		for(int i = 0 ; i < tailles.length ; i++) {
			
			resulatTasMin[i] = complex.complexConsIterTasMin(tailles[i]);
			
			resulatFileBinomial[i] = complex.complexConsIterTasBinomial(tailles[i]);
		}
		
		
		System.out.println("\n----------- complexité ConsIter tas min via Tableau: -----------\n");

		
		for(int i  = 0 ; i < tailles.length ; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulatTasMin[i]+" microseconds ");
		}

		System.out.println("\n\n----------- complexité ConsIter file Binomial : -----------\n");

		
		for(int i  = 0 ; i < tailles.length ; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulatFileBinomial[i]+" microseconds ");
		}
	


	}

	
	
	
	

}
