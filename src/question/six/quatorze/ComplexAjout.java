package question.six.quatorze;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;

import tasBinomial.TasBinomial;
import tatPriorite.min.TasMin;
import question.un.*;

public class ComplexAjout {
	


	Echauffement echauffement  ;
	int[] nombrePercolation;
	static int index = 0; 


	public ComplexAjout() {
		echauffement = new Echauffement() ;
		nombrePercolation = new int[8] ;

	}
	
	
	public String complexAjoutTasMin(int nbCle) {
		
		long time_before ; 
		
		long time_after ;
		
		TasMin tasMin = new TasMin(); 
							
				String fileName = "src/cles_alea/jeu_1_nb_cles_"+nbCle+".txt" ; 
				
				BigInteger[] liste = echauffement.lecteurFichier(fileName);

				tasMin.setItems(tasMin.consIter2(liste, nbCle));
				
				tasMin.setTaille(nbCle);
				
				time_before = System.nanoTime();
				
				tasMin.setNombrePercolation(0);
				
				tasMin.ajout(new BigInteger("0x0000000000000000000000000000000".substring(2),16));
				
				int nbPercolation = tasMin.getNombrePercolation() ;

				time_after = System.nanoTime();
						
				long x = (time_after - time_before)/1000; 
				
				nombrePercolation[index] = nbPercolation ;		
				index++;
				
			return x+""; 
		}
	
	public String complexAjoutTasBinomial(int nbCle) {
		
		long time_before ; 
		long time_after ;
		
		TasBinomial tasBinomial = new TasBinomial(); 
							
				String fileName = "src/cles_alea/jeu_1_nb_cles_"+nbCle+".txt" ; 
				
				BigInteger[] liste = echauffement.lecteurFichier(fileName);
				
				tasBinomial.consIter(liste, nbCle);

				time_before = System.nanoTime();
				
				tasBinomial.ajout(new BigInteger("0x00000000000000000000000000000000".substring(2),16));
				
				time_after = System.nanoTime();
						
				long x = (time_after - time_before)/1000; 
																	
			return x+""; 
		}
		
	
	public static void main(String[] args) {
		
		String [] resulatTasMin = new String[8] ;
		String [] resulatFileBinomial = new String[8] ;
		
		int[] tailles = {100,200,500,1000,5000,10000,20000,50000};
		
		ComplexAjout complex = new ComplexAjout(); 
			
		for(int i = 0 ; i < tailles.length ; i++) {
			resulatTasMin[i] = complex.complexAjoutTasMin(tailles[i]);
			
			resulatFileBinomial[i] = complex.complexAjoutTasBinomial(tailles[i]);
		}

		System.out.println("\n----------- complexité ajout tas min via Tableau: -----------\n");

		
		for(int i  = 0 ; i < tailles.length ; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulatTasMin[i]+" microseconds | nombre de Percolation = "+complex.nombrePercolation[i]);
		}

		System.out.println("\n\n----------- complexité ajout file Binomial : -----------\n");

		
		for(int i  = 0 ; i < tailles.length ; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulatFileBinomial[i]+" microseconds ");
		}
					

	}

		
	
	
	

}
