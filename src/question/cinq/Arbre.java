package question.cinq;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import Tas_minarbre.echauf;

public class Arbre {
	private BigInteger valeur;
	private Arbre gauche, droit;

	// CONSTRUCTEURS
	public Arbre(BigInteger x) {
		valeur = x;
	}

	public Arbre() {
	};

	public Arbre(BigInteger x, Arbre g, Arbre d) {
		valeur = x;
		gauche = g;
		droit = d;
	}

	// ACCESSEURS
	public BigInteger getValeur() {
		return (valeur);
	}

	public Arbre getSousArbreGauche() {
		return (gauche);
	}

	public Arbre getSousArbreDroit() {
		return (droit);
	}

	// AFFICHAGE
	public String toString() {
		return toString("\t");
	}

	public String toString(String s) {
		if (gauche != null) {
			if (droit != null)
				return (s + valeur + "\n" + gauche.toString(s + "\t") + droit.toString(s + "\t"));
			else
				return (s + valeur + "\n" + gauche.toString(s + "\t") + "\n");
		} else if (droit != null)
			return (s + valeur + "\n\n" + droit.toString(s + "\t"));
		else
			return (s + valeur + "\n");
	}

	/**
	 * Affiche l'arbre selon un parcours prefixe
	 */
	public void ParcoursPrefixe() {
		System.out.println(getValeur());
		if (getSousArbreGauche() != null)
			getSousArbreGauche().ParcoursPrefixe();
		if (getSousArbreDroit() != null)
			getSousArbreDroit().ParcoursPrefixe();
	}

	/**
	 * Affiche l'arbre selon un parcours infixe
	 */
	public void ParcoursInfixe() {
		if (getSousArbreGauche() != null)
			getSousArbreGauche().ParcoursInfixe();
		System.out.println(getValeur());
		if (getSousArbreDroit() != null)
			getSousArbreDroit().ParcoursInfixe();
	}

	/**
	 * Affiche l'arbre selon un parcours postfixe
	 */
	public void ParcoursPostfixe() {
		if (getSousArbreGauche() != null)
			getSousArbreGauche().ParcoursPostfixe();
		if (getSousArbreDroit() != null)
			getSousArbreDroit().ParcoursPostfixe();
		System.out.println(getValeur());
	}

	/**
	 * Teste si deux arbres sont egaux, meme valeurs et meme disposition
	 * 
	 * @param a
	 *            l'arbre a comparer a b
	 * @param b
	 *            l'arbre a comparer a a
	 * @return un boolean indiquant si les arbres sont egaux
	 */
	public static boolean arbresEgaux(Arbre a, Arbre b) {
		if ((a == null) && (b == null))
			return true;
		if ((a == null) && (b != null))
			return false;
		if ((a != null) && (b == null))
			return false;

		// A ce point, a et b != null, on peut acceder a leurs champs
		if (a.getValeur() != b.getValeur())
			return false;
		return (arbresEgaux(a.getSousArbreGauche(), b.getSousArbreGauche())
				&& arbresEgaux(a.getSousArbreDroit(), b.getSousArbreDroit()));
	}

	/**
	 * @param a
	 *            un arbre
	 * @return la hauteur de l'arbre a
	 */
	public static int hauteur(Arbre a) {
		if (a == null)
			return 0;
		else
			return (1 + Math.max(hauteur(a.getSousArbreGauche()), hauteur(a.getSousArbreDroit())));
	}

	/**
	 * @param a
	 *            un arbre
	 * @return un boolean indiquant si a est un arbre binaire de recherche
	 */
	public static boolean estABR(Arbre a) {
		if (a == null)
			return true;
		if ((a.getSousArbreGauche() != null) && echauf.sup(a.getSousArbreGauche().getValeur(), a.getValeur()))
			return false;
		if ((a.getSousArbreDroit() != null) && echauf.sup(a.getValeur(), a.getSousArbreDroit().getValeur()))
			return false;
		return (estABR(a.getSousArbreGauche()) && estABR(a.getSousArbreDroit()));
	}

	/**
	 * @param value
	 *            la valeur a recherche dans l'arbre
	 * @return un boolean indiquant si value a ete trouve ou non
	 */
	public boolean recherche(BigInteger value) {
		if (value == getValeur())
			return true;
		if (echauf.inf(value, getValeur()) && (getSousArbreGauche() != null))
			return (getSousArbreGauche().recherche(value));
		if (echauf.sup(value, getValeur()) && (getSousArbreDroit() != null))
			return (getSousArbreDroit().recherche(value));
		return false;
	}

	/**
	 * @param value
	 *            la valeur a inserer dans l'arbre
	 */
	public void insertion(BigInteger value) {
		if (value == getValeur())
			return; // la valeur est deja dans l'arbre
		if (echauf.inf(value, getValeur())) {
			if (getSousArbreGauche() != null)
				getSousArbreGauche().insertion(value);
			else
				gauche = new Arbre(value);
		}
		if (echauf.sup(value, getValeur())) {
			if (getSousArbreDroit() != null)
				getSousArbreDroit().insertion(value);
			else
				droit = new Arbre(value);
		}
	}


	
}