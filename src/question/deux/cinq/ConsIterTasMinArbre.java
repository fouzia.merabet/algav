package question.deux.cinq;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

import Tas_minarbre.CalculeComplexite;
import Tas_minarbre.Tas_min;
import Tas_minarbre.echauf;

public class ConsIterTasMinArbre {
	
	

	private static final int NB_CLES_MAX = 50000;
	public  int nbCle = 0 ;
	
	public String complexiteMoyenne(int nbCle) {
			
	long time = 0 ; 
	long time_before ; 
	long time_after ;
	
	Tas_min tasMin = new Tas_min(); 

		for(int  i = 0 ; i < 5 ; i++) {
			
			String fileName = "src/cles_alea/jeu_"+(i+1)+"_nb_cles_"+nbCle+".txt" ; 
			
			List <BigInteger> liste = echauf.lecteur(fileName);
						
			time_before = System.nanoTime();

			tasMin.construction(liste);
					
			time_after = System.nanoTime();
					
			long x = (time_after - time_before)/1000; 
			
			time = time + x ;
			
		}
		
	
		time = time / 5;
				
		return time+""; 
	}

	
	public static void main(String[] args) {
		// lecteur des fichier a 100 cles 
	
		int[] tailles = {100,200,500,1000,5000,10000,20000,50000};

		String [] resulat = new String[8] ;

		ConsIterTasMinArbre complex = new ConsIterTasMinArbre() ; 
		
		for(int i = 0 ; i < tailles.length ; i++) {
			resulat[i] = complex.complexiteMoyenne(tailles[i]);
		}
		
		System.out.println("\n----------- complexité ConsIter tas min via Arbre: -----------\n");
		
		for(int i  = 0 ; i < tailles.length ; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulat[i]+" microseconds ");
			System.out.print("\n--------------------------------------------------------------------------------------\n");
		}
		
		
		
	}


}
