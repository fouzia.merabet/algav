package Tas_minarbre;

import java.math.BigInteger;
import java.util.*;

import Tas_minarbre.echauf;

public class Tas_min {
	Noeud racine;
	Noeud r = new Noeud();// pere
	Noeud F2 = new Noeud();// dernier
	Noeud F1 = new Noeud();// avant derenier
	Noeud F;// le premier dans un niveau
	int taille;
	int i;

	public Tas_min() {
	}

	public void construction(List<BigInteger> list) {

		BigInteger val = list.get(0);
		racine = new Noeud(val);
		racine.Pere = null;
		r = racine;
		F1 = racine;
		F2 = racine;
		F = F2;
		taille = list.size();
		for (i = 1; i < list.size(); i++) {
			BigInteger val1 = list.get(i);
			Ajout(val1);
		}
	}

	public void remonte(Noeud N) {
		BigInteger A;
 
		while (echauf.inf(N.Cle, N.Pere.Cle) == true) {
			
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			if (N.Pere.Cle != null) {
				A = N.Cle;
				N.Cle = N.Pere.Cle;
				N.Pere.Cle = A;
				N = N.Pere;
				if (N.Pere == null) {
					break;
				}
			} else {
				break;
			}
		}
		

	}

	public void Ajout(BigInteger val1) {
		F2 = new Noeud(val1);

		if (echauf.est_vide(r.Gauche)) {
			F1.Voisin = F2;
			r.Gauche = F2;
			F2.Pere = r;
			F1 = F2;

		} else {
			if (echauf.est_vide(r.Droite))

			{
				F1.Voisin = F2;
				r.Droite = F2;
				F2.Pere = r;
				F1 = F2;
			} else {
				if (echauf.est_vide(r.Voisin)) {
					F1.Voisin = null;
					F.Gauche = F2;
					F2.Pere = F;
					F = F2;
				} else {
					F1.Voisin = F2;
					r = r.Voisin;
					r.Gauche = F2;
					F2.Pere = r;
					F1 = F2;
				}

			}

		}
		remonte(F2);
	}

	public Noeud remonte_min(Noeud s) {
		Noeud m;
		if (echauf.inf(s.Gauche.Cle, s.Droite.Cle)) {
			s.Cle = s.Gauche.Cle;
			m = s.Gauche;
			return (m);

		} else {
			s.Cle = s.Droite.Cle;
		}
		m = s.Droite;
		return m;

	}

	public void SupprMin() {
		Noeud m;
		racine.Cle = null;
		m = remonte_min(racine);
		while (m.Gauche.Cle != null) {
			m = remonte_min(m);
		}
		m.Cle = F2.Cle;
		F2.Cle = null;

	}

	public void ajout_rec(Noeud I) {
		if (I.Cle != null) {
			Ajout(I.Cle);
			ajout_rec(I.Gauche);
			ajout_rec(I.Droite);
		} else {
			return;
		}

	}
	public Tas_min union(Tas_min T) {
        Tas_min Tm=new Tas_min();
		Tm=T;
		Tm.ajout_rec(racine);
		return  (Tm);

	}

	public void affiche_rc(Noeud k) {
		if (k.Cle != null) {
			System.out.println(k.Cle);
		}

		if (k.Gauche != null) {

			affiche_rc(k.Gauche);

		}
		if (k.Droite != null) {

			affiche_rc(k.Droite);
		}

	}

	public void affiche() {
		affiche_rc(racine);
	}

}
