package question.deux.six;

import java.math.BigInteger;
import java.util.concurrent.TimeUnit;

import com.google.common.base.Stopwatch;

import question.deux.cinq.ConsIterTasMinTableau;
import question.un.Echauffement;
import tatPriorite.min.TasMin;

public class UnionTasMinTableau {

	Echauffement echauffement;
	
	int[] nombrePercolation; 
	
	static int index = 0; 

	public UnionTasMinTableau() {
		echauffement = new Echauffement();
		nombrePercolation = new int[8] ;
	}
	
	

	public String complexiteMoyenne(int nbCle) {

		long time_before;
		long time_after;

		TasMin tasMin1 = new TasMin();
		TasMin tasMin2 = new TasMin();
		TasMin tasMin3 = new TasMin();

		String fileName1 = "src/cles_alea/jeu_1_nb_cles_" + nbCle + ".txt";
		String fileName2 = "src/cles_alea/jeu_2_nb_cles_" + nbCle + ".txt";

		BigInteger[] liste1 = echauffement.lecteurFichier(fileName1);
		BigInteger[] liste2 = echauffement.lecteurFichier(fileName2);


		tasMin1.consIter1(liste1, nbCle);
				
		tasMin2.consIter1(liste2, nbCle);

		time_before = System.nanoTime();

		tasMin3.setNombrePercolation(0);
		
		tasMin3.Union(tasMin1.getItems(), tasMin1.getTaille(), tasMin2.getItems(), tasMin2.getTaille());

		
		nombrePercolation[index] = tasMin3.getNombrePercolation() ; 
		index ++;
		
		time_after = System.nanoTime();

		long x = (time_after - time_before);

		return x + "";
	}

	public static void main(String[] args) {

		int[] tailles = { 100, 200, 500, 1000, 5000, 10000, 20000, 50000 };

		String[] resulat = new String[8];

		UnionTasMinTableau complex = new UnionTasMinTableau();

		for (int i = 0; i < tailles.length; i++) {
			resulat[i] = complex.complexiteMoyenne(tailles[i]);
		}

		System.out.println("\n----------- complexité Union tas min via Tableau: -----------\n");

		for (int i = 0; i < tailles.length; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulat[i]+" milliseconds | moyenne de Percolation = "+complex.nombrePercolation[i]);
			System.out.print("\n--------------------------------------------------------------------------------------\n");
		
		}

	}

}
