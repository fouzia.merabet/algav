package Tas_minarbre;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class echauf {
	
	
	public static  List<BigInteger> lecteur(String nomFichier) {
		   
		   List<BigInteger> list = new ArrayList<BigInteger>();
			
			try
		     {
		         File f = new File (nomFichier);
		         FileReader fr = new FileReader (f);
		         BufferedReader br = new BufferedReader (fr);
		      
		         try
		         {
		             String line = br.readLine();
		          
		      
		             while (line != null)
		             {
		            	 list.add(new BigInteger(line.substring(2), 16));
		                 line = br.readLine();

		             }
		      
		             br.close();
		             fr.close();
		         }
		         catch (IOException exception)
		         {
		             System.out.println ("Erreur lors de la lecture : " + exception.getMessage());
		         }
		     }
		     catch (FileNotFoundException exception)
		     {
		         System.out.println ("Le fichier n'a pas été trouvé");
		     }
			
			return list;
		}
	
	
	public static boolean inf(BigInteger cle1 , BigInteger cle2) {

		int resultat = cle1.compareTo(cle2);
		if(resultat ==-1) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public static boolean infOrEq(BigInteger cle1 , BigInteger cle2) {

		int resultat = cle1.compareTo(cle2);
		if(resultat ==-1 || resultat == 0) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public static boolean eg(BigInteger cle1 , BigInteger cle2) {

		int resultat = cle1.compareTo(cle2);
		if(resultat == 0) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public static boolean sup(BigInteger cle1 , BigInteger cle2) {
		
		int resultat = cle1.compareTo(cle2);
		if(resultat == 1) {
			return true;
		}else {
			return false;
		}
	}
	
	public static boolean est_vide(Noeud x) {
		return x.Cle == null;
	}
}
