package Tas_minarbre;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

public class CalculeComplexite {
 
	
	private static final int NB_CLES_MAX = 50000;
	public  int nbCle = 0 ;
	
	public String complexiteMoyenne(int nbCle) {
			
	double time = 0 ; 
	double time_before ; 
	double time_after ;
	
	Tas_min tasMin = new Tas_min(); 

		for(int  i = 0 ; i < 5 ; i++) {
			
			String fileName = "/home/fouzia/Téléchargements/partie_fouzia/src/projet_algav/cles_alea/jeu_"+(i+1)+"_nb_cles_"+nbCle+".txt" ; 
			
			List <BigInteger> liste = echauf.lecteur(fileName);
			
			
			time_before = System.nanoTime();

			tasMin.construction(liste);
			
		
			
			time_after = System.nanoTime();
					
			double x = (time_after - time_before)/1000000.0; 
			
			time = time + x ;
			
		}
		
	
		time = time / 5;
				
		return time+""; 
	}

	
	public  void usingBufferedWritter(String fileName , String[] line) throws IOException
	{
    
	    
	    //"ResultatTest/tasMinTabConsIter.txt"
	    BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));

	    for(int i  = 0 ; i< line.length ; i++) {
	    	
	    	writer.write(line[i]);
		    writer.newLine();
	    }
	    
	 
	    writer.close();
	}
	
	public static void main(String[] args) {
		// lecteur des fichier a 100 cles 
	
		String [] resulat = new String[8] ;
		
		CalculeComplexite c = new  CalculeComplexite() ; 
		
		String complexite50000  = c.complexiteMoyenne(50000) ;
		resulat[7] = complexite50000; 
		System.out.println("comlex 50000 = "+complexite50000);
		

		String complexite20000  = c.complexiteMoyenne(20000) ;
		resulat[6] = complexite20000; 
		System.out.println("comlex 20000 = "+complexite20000);
		

		String complexite10000  = c.complexiteMoyenne(10000) ;
		resulat[5] = complexite10000; 
		System.out.println("comlex 10000 = "+complexite10000);
		

		String complexite5000  = c.complexiteMoyenne(5000) ;
		resulat[4] = complexite5000; 
		System.out.println("comlex 5000 = "+complexite5000);

		String complexite1000  = c.complexiteMoyenne(1000) ;
		resulat[3] = complexite1000; 
		System.out.println("comlex 1000 = "+complexite1000);

		String complexite500  = c.complexiteMoyenne(500) ;
		resulat[2] = complexite500; 
		System.out.println("comlex 5\n" + 
				"	public00 = "+complexite500);
		

		String complexite200  = c.complexiteMoyenne(200) ;
		resulat[1] = complexite200; 
		System.out.println("comlex 200 = "+complexite200);
		
		String complexite100  = c.complexiteMoyenne(100) ;
		resulat[0] = complexite100;
		System.out.println("comlex 100 = "+complexite100);

		try {
			c.usingBufferedWritter("ResultatTest/tasMinarb"
					+ "ConsIter.txt" , resulat );
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	
	
	
}
