package question.six.douze;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import question.cinq.Arbre;
import question.quatre.md5;

public class testmd5 {
	public static List<String> lecteur(String nomFichier) {

		List<String> list = new ArrayList<String>();

		try {
			File f = new File(nomFichier);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			try {
				String line = br.readLine();

				while (line != null) {
					list.add(new String(line));
					line = br.readLine();

				}

				br.close();
				fr.close();
			} catch (IOException exception) {
				System.out.println("Erreur lors de la lecture : " + exception.getMessage());
			}
		} catch (FileNotFoundException exception) {
			System.out.println("Le fichier n'a pas été trouvé");
		}

		return list;
	}

	public static void main(String[] args) {
		List<String> messages = lecteur("src/Shakespeare/1henryiv.txt");
		List<String> m = new ArrayList<String>();

		int i = 0;
		for (String message : messages) {
			m.add(md5.md5DigestHexString(message));

		}
		BigInteger a = new BigInteger(m.get(0), 16);
		Arbre A = new Arbre(a);
		for (i = 1; i < m.size(); i++) {
			A.insertion(new BigInteger(m.get(i), 16));

		}
		A.ParcoursPostfixe();
		

	}
}
