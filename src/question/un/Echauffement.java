package question.un;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.concurrent.TimeUnit;

public class Echauffement {

	private static int NB_CLES_MAX = 100000;

	public static int nbCle = 0;

	public void testWait() {
		final long INTERVAL = 1000;
		long start = System.nanoTime();
		long end = 0;
		do {
			end = System.nanoTime();
		} while (start + INTERVAL >= end);
	}

	public static boolean inf(BigInteger cle1, BigInteger cle2) {

		int resultat = cle1.compareTo(cle2);
		if (resultat == -1) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean infOrEq(BigInteger cle1, BigInteger cle2) {

		int resultat = cle1.compareTo(cle2);
		if (resultat == -1 || resultat == 0) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean eg(BigInteger cle1, BigInteger cle2) {

		int resultat = cle1.compareTo(cle2);
		if (resultat == 0) {
			return true;
		} else {
			return false;
		}

	}

	public static boolean sup(BigInteger cle1, BigInteger cle2) {

		int resultat = cle1.compareTo(cle2);
		if (resultat == 1) {
			return true;
		} else {
			return false;
		}
	}

	// lecture du fichier qui va retourner une liste de bigInteger en décimal

	public BigInteger[] lecteurFichier(String nomFichier) {

		BigInteger[] liste = new BigInteger[NB_CLES_MAX];

		nbCle = 0;

		try {
			File f = new File(nomFichier);
			FileReader fr = new FileReader(f);
			BufferedReader br = new BufferedReader(fr);

			try {
				String line = br.readLine();

				while (line != null) {
					liste[nbCle] = new BigInteger(line.substring(2), 16);
					line = br.readLine();
					nbCle++;

				}

				br.close();
				fr.close();
			} catch (IOException exception) {
				System.out.println("Erreur lors de la lecture : " + exception.getMessage());
			}
		} catch (FileNotFoundException exception) {
			System.out.println("Le fichier n'a pas été trouvé");
		}

		return liste;
	}

	// méthode qui perment d'écrie dans un fichier les résultats obtenu

	public void usingBufferedWritter(String fileName, String[] lines) throws IOException {

		BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));

		for (int i = 0; i < lines.length; i++) {

			writer.write(lines[i]);

			writer.newLine();
		}

		writer.close();
	}

	public static void main(String[] args) {

	}

}
