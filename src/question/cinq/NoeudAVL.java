package question.cinq;

public class NoeudAVL {
	   Comparable element;
	   NoeudAVL gauche, droit;
	   int equilibre;
	   public NoeudAVL(){
	       gauche = null;
	       droit = null;
	       equilibre = 0;
	   }
	   public NoeudAVL(Comparable e, NoeudAVL g, NoeudAVL d){
	      element = e;
	      gauche = g;
	      droit = d;
	      equilibre = 0;
	   }

}
