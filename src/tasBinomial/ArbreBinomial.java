package tasBinomial;

import java.math.BigInteger;

import question.un.*;;

class ArbreBinomial {

	private int degree;
	private BigInteger cle;
	private ArbreBinomial parent;
	private ArbreBinomial frere;
	private ArbreBinomial enfant;

	public ArbreBinomial(BigInteger k) {
		cle = k;
		degree = 0;
		parent = null;
		frere = null;
		enfant = null;
	}

	public ArbreBinomial inverser(ArbreBinomial sibl) {

		ArbreBinomial ret;

		if (frere != null)
			ret = frere.inverser(this);
		else
			ret = this;
		frere = sibl;
		return ret;
	}

	public int getTaille() {
		return (1 + ((enfant == null) ? 0 : enfant.getTaille()) + ((frere == null) ? 0 : frere.getTaille()));
	}

	// Getters and Setters
	public int getDegree() {
		return degree;
	}

	public void setDegree(int degree) {
		this.degree = degree;
	}

	public BigInteger getCle() {
		return cle;
	}

	public void setCle(BigInteger key) {
		this.cle = key;
	}

	public ArbreBinomial getParent() {
		return parent;
	}

	public void setParent(ArbreBinomial parent) {
		this.parent = parent;
	}

	public ArbreBinomial getFrere() {
		return frere;
	}

	public void setFrere(ArbreBinomial sibling) {
		this.frere = sibling;
	}

	public ArbreBinomial getEnfant() {
		return enfant;
	}

	public void setEnfant(ArbreBinomial child) {
		this.enfant = child;
	}
}
