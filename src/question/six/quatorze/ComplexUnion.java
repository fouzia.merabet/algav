package question.six.quatorze;

import java.math.BigInteger;
import java.util.Iterator;

import question.un.Echauffement;
import tasBinomial.TasBinomial;
import tatPriorite.min.TasMin;

public class ComplexUnion {
	
	Echauffement echauffement  ;
	
	public  ComplexUnion() {
		echauffement = new Echauffement() ;
	}	

		
	public String complexUnionTasMin(int nbCle) {
		
		double time_before ; 
		double time_after ;
		
		TasMin tasMin1 = new TasMin(); 
		TasMin tasMin2 = new TasMin(); 
		TasMin tasMin3 = new TasMin(); 
		
		String fileName1 = "src/cles_alea/jeu_1_nb_cles_"+nbCle+".txt" ; 
		String fileName2 = "src/cles_alea/jeu_4_nb_cles_"+nbCle+".txt" ; 
				
		BigInteger[] liste1 = echauffement.lecteurFichier(fileName1);
		BigInteger[] liste2 = echauffement.lecteurFichier(fileName2);
				
		tasMin1.setItems(tasMin1.consIter2(liste1, nbCle));
		tasMin1.setTaille(nbCle);
		
		tasMin2.setItems(tasMin2.consIter2(liste2, nbCle));
		tasMin2.setTaille(nbCle);
		

		time_before = System.nanoTime();
		
		tasMin3.setNombrePercolation(0);

		tasMin3.Union(tasMin1.getItems(), tasMin1.getTaille(), tasMin2.getItems(), tasMin2.getTaille());		

		time_after = System.nanoTime();
				
		double x = (time_after - time_before)/1000.0; 
	
			return x+""; 
		}
	
	
	public String complexUnionTasBinomial(int nbCle) {
		
		double time_before ; 
		double time_after ;
		
		TasBinomial tasBinomial1 = new TasBinomial(); 
		TasBinomial tasBinomial2 = new TasBinomial(); 
		
		String fileName1 = "src/cles_alea/jeu_1_nb_cles_"+nbCle+".txt" ; 
		String fileName2 = "src/cles_alea/jeu_2_nb_cles_"+nbCle+".txt" ; 
				
		BigInteger[] liste1 = echauffement.lecteurFichier(fileName1);
		BigInteger[] liste2 = echauffement.lecteurFichier(fileName2);
				
		tasBinomial1.consIter(liste1, nbCle);
		tasBinomial2.consIter(liste2, nbCle);
		

		time_before = System.nanoTime();
		
		tasBinomial1.union(tasBinomial2.getNoeuds());
		
		time_after = System.nanoTime();
						
		double x = (time_after - time_before)/1000.0; 
	
			return x+""; 
		}
	
	
	public static void main(String[] args) {
		
		
		String [] resulatTasMin = new String[8] ;
		
		String [] resulatFileBinomial = new String[8] ;
		
		int[] tailles = {100,200,500,1000,5000,10000,20000,50000};
		
		ComplexUnion complex = new ComplexUnion() ; 

		
		for(int i = 0 ; i < tailles.length ; i++) {
			
			resulatTasMin[i] = complex.complexUnionTasMin(tailles[i]);
			
			resulatFileBinomial[i] = complex.complexUnionTasBinomial(tailles[i]);
		}
		
	System.out.println("\n----------- complexité Union tas min via Tableau: -----------\n");

		
		for(int i  = 0 ; i < tailles.length ; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulatTasMin[i]+" microseconds ");
		}

		System.out.println("\n\n----------- complexité Union file Binomial : -----------\n");

		
		for(int i  = 0 ; i < tailles.length ; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulatFileBinomial[i]+" microseconds ");
		}
		
				   
	}

}
