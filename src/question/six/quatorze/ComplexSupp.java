package question.six.quatorze;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigInteger;

import question.un.Echauffement;
import tasBinomial.TasBinomial;
import tatPriorite.min.TasMin;

public class ComplexSupp {

	Echauffement echauffement  ;	
	int[] nombrePercolation;
	static int index = 0; 
	
	public ComplexSupp() {
		echauffement = new Echauffement() ;
		nombrePercolation = new int[8] ;
	}	
	
	public String complexSuppTasMin(int nbCle) {
		
		long time_before ; 
		long time_after ;
		
		TasMin tasMin = new TasMin(); 
							
				String fileName = "src/cles_alea/jeu_1_nb_cles_"+nbCle+".txt" ; 
				
				BigInteger[] liste = echauffement.lecteurFichier(fileName);

				//tasMin.consIter2(liste, nbCle);
				
				tasMin.setItems(tasMin.consIter2(liste, nbCle));
				tasMin.setTaille(nbCle);
				
				time_before = System.nanoTime();
				tasMin.setNombrePercolation(0);

				tasMin.suppMin();
				
				int nbPercolation = tasMin.getNombrePercolation() ;

				time_after = System.nanoTime();
						
				long x = (time_after - time_before)/1000; 
				
				nombrePercolation[index] = nbPercolation ;		
				index++;
				
										
			return x+""; 
		}
	
	public String complexSuppTasBinomial(int nbCle) {
		
		long time_before ; 
		long time_after ;
		
		TasBinomial tasBinomial = new TasBinomial(); 
							
				String fileName = "src/cles_alea/jeu_1_nb_cles_"+nbCle+".txt" ; 
				
				BigInteger[] liste = echauffement.lecteurFichier(fileName);
				
				tasBinomial.consIter(liste, nbCle);

				time_before = System.nanoTime();
				
				tasBinomial.supprMin() ; 
				
				time_after = System.nanoTime();
						
				long x = (time_after - time_before)/1000; 
																	
			return x+""; 
			
		}
	
	public static void main(String[] args) {
		

		String [] resulatTasMin = new String[8] ;
		
		String [] resulatFileBinomial = new String[8] ;
		
		int[] tailles = {100,200,500,1000,5000,10000,20000,50000};
		
		ComplexSupp complex = new ComplexSupp(); 
		
	
		for(int i = 0 ; i < tailles.length ; i++) {
			
			resulatTasMin[i] = complex.complexSuppTasMin(tailles[i]);
			
			resulatFileBinomial[i] = complex.complexSuppTasBinomial(tailles[i]);
		}
		
	System.out.println("\n----------- complexité Supp tas min via Tableau: -----------\n");

		
		for(int i  = 0 ; i < tailles.length ; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulatTasMin[i]+" microseconds | nombre de Percolation = "+complex.nombrePercolation[i]);
		}

		System.out.println("\n\n----------- complexité Supp file Binomial : -----------\n");

		
		for(int i  = 0 ; i < tailles.length ; i++) {
			System.out.println("\nliste à "+tailles[i]+" cles : "+resulatFileBinomial[i]+" microseconds ");
		}
		
	}
		
}
