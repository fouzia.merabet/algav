package tatPriorite.min;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import question.un.*;

public class TasMin {

	public static final int CAPACITE_TAS = 100000;

	private int capacite;
	private int taille;
	private BigInteger[] items;
	private int nombrePercolation;

	public TasMin() {
		this.capacite = CAPACITE_TAS;
		taille = 0;
		nombrePercolation = 0;
		items = new BigInteger[CAPACITE_TAS];
		Arrays.fill(items, BigInteger.ZERO);
	}

	public TasMin(int capacite, int taille, BigInteger[] items) {
		// super();
		this.capacite = capacite;
		this.taille = taille;
		this.items = items;

	}

	public void suppMin() {
		if (taille == 0)
			throw new IllegalStateException();
		items[0] = items[taille - 1];

		percolationEnBas();

	}

	private void percolationEnBas() {

		int index = 0;

		while (aUnEnfantGauche(index)) {

			int smallerChildIndex = getEnfantGaucheIndex(index);

			if (aUnEnfantDroite(index) && Echauffement.inf(enfantDroite(index), enfantGauche(index))) {
				smallerChildIndex = getEnfantDroitIndex(index);
			}

			if (Echauffement.inf(items[index], items[smallerChildIndex])) {
				break;
			} else {

				permutation(index, smallerChildIndex);
				nombrePercolation++;
			}

			index = smallerChildIndex;

			taille--;

		}

	}

	public void ajout(BigInteger item) {

		augmenterCapacite();
		items[taille] = item;
		taille++;
		percolatioEnHaut();

	}

	public void testWait() {
		final long INTERVAL = 2;
		long start = System.nanoTime();
		long end = 0;
		do {
			end = System.nanoTime();
		} while (start + INTERVAL >= end);
	}

	private void percolatioEnHaut() {

		
		int index = taille - 1;

		while (aUnParent(index) && Echauffement.inf(items[index], parent(index))) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			permutation(getParentIndex(index), index);
			index = getParentIndex(index);
			nombrePercolation++;
		}

	}

	public void ajout2(BigInteger item) {

		augmenterCapacite();

		items[taille] = item;

		taille++;

		percolatioEnHaut2();
	}

	private void percolatioEnHaut2() {

		int index = taille - 1;

		while (aUnParent(index) && Echauffement.inf(items[index], parent(index))) {
			permutation(getParentIndex(index), index);
			index = getParentIndex(index);

		}

	}

	/*
	 * Cette méthode permet de de construire itérativement un tas à partir d’une
	 * liste d’éléments (par ajout successifs)
	 */
	public void consIter1(BigInteger listeElement[], int taille) {

		items = new BigInteger[CAPACITE_TAS];

		Arrays.fill(items, BigInteger.ZERO);

		this.taille = 0;

		nombrePercolation = 0 ; 
		
		for (int i = 0; i < taille; i++) {
				
			ajout(listeElement[i]);
		}

	}

	/*
	 * Cette méthode repose sur l'algorithme proposé dans le livre Cormen
	 * introduction à l'algorithmique Une feuille est déja un TAS-MIN On démarre
	 * donc depuis le noeud qui n'est pas un TAS-MIN et qui a le plus grand indice
	 * jusqu'au ROOT Donc depuis l'indice i = Math.round((taille/2))-1 à i = 0 et on
	 * fais un appel à la méhode entasserMin()
	 */
	public BigInteger[] consIter2(BigInteger[] arr, float taille) {

		for (int i = Math.round((taille / 2)) - 1; i >= 0; i--) {
			percolatioMin(arr, (int) taille, i);

		}

		return arr;
	}

	/*
	 * Le principe de cette méthode est simple, on fusionne les deux tableaux (les
	 * deux TAS-MIN) On applique le même principe de consIter2
	 */
	public void Union(BigInteger[] tab1, int tailleTab1, BigInteger[] tab2, int tailleTab2) {

		items = new BigInteger[CAPACITE_TAS];
		Arrays.fill(items, BigInteger.ZERO);

		for (int i = 0; i < tailleTab1; i++) {
			items[i] = tab1[i];
		}

		for (int i = 0; i < tailleTab2; i++) {
			items[tailleTab1 + i] = tab2[i];
		}
		this.taille = tailleTab1 + tailleTab2;

	
		nombrePercolation = 0;
		

		for (int i = this.taille / 2 - 1; i >= 0; i--) {
			
			percolatioMin(items, taille, i);
		}

	}

	private void percolatioMin(BigInteger[] tab, int n, int i) {
		if (i >= n) {
			return;
		}

		int gauche = i * 2 + 1;
		int droit = i * 2 + 2;
		int min;

		if (gauche < n && Echauffement.inf(tab[gauche], tab[i])) {
			min = gauche;
		} else
			min = i;
		if (droit < n && Echauffement.inf(tab[droit], tab[min])) {
			min = droit;
		}

		if (min != i) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			nombrePercolation++;
			BigInteger temp = tab[min];
			tab[min] = tab[i];
			tab[i] = temp;
			percolatioMin(tab, n, min);
		}
	}

	public void affichageTasMin() {

		System.out.print("\n  ");
		for (int i = 0; i < taille; i++) {
			System.out.print(items[i] + "   ");
		}
		System.out.print("\n");

	}

	private int getEnfantGaucheIndex(int parentIndex) {
		return 2 * parentIndex + 1;
	}

	private int getEnfantDroitIndex(int parentIndex) {
		return 2 * parentIndex + 2;
	}

	private int getParentIndex(int childIndex) {
		return (childIndex - 1) / 2;
	}

	private boolean aUnEnfantGauche(int index) {
		return getEnfantGaucheIndex(index) < taille;
	}

	private boolean aUnEnfantDroite(int index) {
		return getEnfantDroitIndex(index) < taille;
	}

	private boolean aUnParent(int index) {
		return getParentIndex(index) >= 0;
	}

	private BigInteger enfantGauche(int index) {
		return items[getEnfantGaucheIndex(index)];
	}

	private BigInteger enfantDroite(int index) {
		return items[getEnfantDroitIndex(index)];
	}

	private BigInteger parent(int index) {
		return items[getParentIndex(index)];
	}

	private void permutation(int index1, int index2) {

		BigInteger autre = items[index1];
		items[index1] = items[index2];
		items[index2] = autre;
	}

	public BigInteger sommet() {
		if (taille == 0)
			throw new IllegalStateException();
		return items[0];

	}

	private void augmenterCapacite() {
		if (taille == capacite) {
			items = Arrays.copyOf(items, capacite * 2);
			capacite *= 2;
		}
	}

	public int getCapacite() {
		return capacite;
	}

	public void setCapacite(int capacite) {
		this.capacite = capacite;
	}

	public int getTaille() {
		return taille;
	}

	public void setTaille(int taille) {
		this.taille = taille;
	}

	public BigInteger[] getItems() {
		return items;
	}

	public void setItems(BigInteger[] items) {
		this.items = items;
	}

	public int getNombrePercolation() {
		return nombrePercolation;
	}

	public void setNombrePercolation(int nombrePercolation) {
		this.nombrePercolation = nombrePercolation;
	}

}
